import React, { useEffect, useState } from 'react';
import { Dimensions, Text, View, Linking, TouchableOpacity } from 'react-native';

export const get_text=(param)=>{

    const dim = [Dimensions.get('window').width, Dimensions.get('window').height]

    switch (param) {
        case "about":
            return(                
            <View style={{width:dim[0]*.9}}>
                <Text style={{color:'#ff0055',fontWeight:'bold', textAlign:'left', fontSize:22, marginVertical:dim[1]/39}}>
                Quellen:
                </Text>
                <Text style={{color:'#0080ff', textAlign:'left', fontWeight:'bold',  fontSize:16, marginBottom:dim[1]/72}}>
                    Förderschwerpunkt Lernen
                </Text>
                <TouchableOpacity onPress={()=>{Linking.openURL('https://www.hamburg.de/contentblob/4632764/bd41f497a879016d7d5e4cf67bea94bf/data/abgrenzung-lse-dl.pdf')}}>
                <Text style={{color:'#0080ff', textAlign:'left', textDecorationLine:'underline', fontSize:16, marginBottom:dim[1]/72, marginLeft:dim[0]/39}}>
                https://www.hamburg.de/contentblob/4632764/bd41f497a879016d7d5e4cf67bea94bf/data/abgrenzung-lse-dl.pdf   
                </Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={()=>{Linking.openURL('https://www.bezreg-muenster.de/zentralablage/dokumente/schule_und_bildung/inklusion/handreichungen_und_leitfaeden/handreichung_fsp_lernen.pdf')}}>
                <Text style={{color:'#0080ff', textAlign:'left', textDecorationLine:'underline', fontSize:16, marginBottom:dim[1]/72, marginLeft:dim[0]/39}}>
                https://www.bezreg-muenster.de/zentralablage/dokumente/schule_und_bildung/inklusion/handreichungen_und_leitfaeden/handreichung_fsp_lernen.pdf   
                </Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={()=>{Linking.openURL('http://gsg.intercoaster.de/icoaster/files/sonderp_d_f_rderschwerpunkte_nrw_stand01_07_2016.pdf')}}>
                <Text style={{color:'#0080ff', textAlign:'left', textDecorationLine:'underline', fontSize:16, marginBottom:dim[1]/72, marginLeft:dim[0]/39}}>
                http://gsg.intercoaster.de/icoaster/files/sonderp_d_f_rderschwerpunkte_nrw_stand01_07_2016.pdf   
                </Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={()=>{Linking.openURL('https://www.schulentwicklung.nrw.de/q/upload/Inklusion/Fachtagung2018/Informationen_zum_Frderschwerpunkt_LE.pdf')}}>
                <Text style={{color:'#0080ff', textAlign:'left', textDecorationLine:'underline', fontSize:16, marginBottom:dim[1]/72, marginLeft:dim[0]/39}}>
                https://www.schulentwicklung.nrw.de/q/upload/Inklusion/Fachtagung2018/Informationen_zum_Frderschwerpunkt_LE.pdf   
                </Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={()=>{Linking.openURL('https://www.ptz-rpi.de/fileadmin/user_upload/ptz/einzelhomepageseite/SBBZ/sbbz-pdf/2016_Zehn_Tipps_SBBZ.pdf')}}>
                <Text style={{color:'#0080ff', textAlign:'left', textDecorationLine:'underline', fontSize:16, marginBottom:dim[1]/72, marginLeft:dim[0]/39}}>
                https://www.ptz-rpi.de/fileadmin/user_upload/ptz/einzelhomepageseite/SBBZ/sbbz-pdf/2016_Zehn_Tipps_SBBZ.pdf   
                </Text>
                </TouchableOpacity>

                <Text style={{color:'#0080ff', textAlign:'left', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/72}}>
                    Förderschwerpunkt Emotionale und soziale Entwicklung
                </Text>
                <TouchableOpacity onPress={()=>{Linking.openURL('https://static1.squarespace.com/static/5bd18e7d840b1647d2bf128b/t/5c6feb5a6e9a7f35d6d32d04/1550838633880/NRW_MSW-Chronisch+kranke+Schülerinnen_allgem.+Schule_HOANZL+Seite+88-95.pdf#page=9')}}>
                <Text style={{color:'#0080ff', textAlign:'left', textDecorationLine:'underline', fontSize:16, marginBottom:dim[1]/72, marginLeft:dim[0]/39}}>
                https://static1.squarespace.com/static/5bd18e7d840b1647d2bf128b/t/5c6feb5a6e9a7f35d6d32d04/1550838633880/NRW_MSW-Chronisch+kranke+Schülerinnen_allgem.+Schule_HOANZL+Seite+88-95.pdf#page=9   
                </Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={()=>{Linking.openURL('https://bildungsserver.berlin-brandenburg.de/fileadmin/bbb/unterricht/sonderpaedagogische_Foerderung_und_gemeinsamer_Unterricht/HR_EmSoz-1.pdf')}}>
                <Text style={{color:'#0080ff', textAlign:'left', textDecorationLine:'underline', fontSize:16, marginBottom:dim[1]/72, marginLeft:dim[0]/39}}>
                https://bildungsserver.berlin-brandenburg.de/fileadmin/bbb/unterricht/sonderpaedagogische_Foerderung_und_gemeinsamer_Unterricht/HR_EmSoz-1.pdf 
                </Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={()=>{Linking.openURL('https://www.kmk.org/fileadmin/veroeffentlichungen_beschluesse/2000/2000_03_10-FS-Emotionale-soziale-Entw.pdf')}}>
                <Text style={{color:'#0080ff', textAlign:'left', textDecorationLine:'underline', fontSize:16, marginBottom:dim[1]/72, marginLeft:dim[0]/39}}>
                https://www.kmk.org/fileadmin/veroeffentlichungen_beschluesse/2000/2000_03_10-FS-Emotionale-soziale-Entw.pdf
                </Text>
                </TouchableOpacity>
                <Text style={{color:'#0080ff', textAlign:'left', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/72}}>
                    Förderschwerpunkt geistige Entwicklung
                </Text>
                <TouchableOpacity onPress={()=>{Linking.openURL('https://broschüren.nrw/sonderpaedagogische-foerderschwerpunkte/home/#!/Home')}}>
                <Text style={{color:'#0080ff', textAlign:'left', textDecorationLine:'underline', fontSize:16, marginBottom:dim[1]/72, marginLeft:dim[0]/39}}>
                https://broschüren.nrw/sonderpaedagogische-foerderschwerpunkte/home/#!/Home   
                </Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={()=>{Linking.openURL('https://www.kmk.org/fileadmin/Dateien/veroeffentlichungen_beschluesse/1998/1998_06_20_FS_Geistige_Entwickl.pdf')}}>
                <Text style={{color:'#0080ff', textAlign:'left', textDecorationLine:'underline', fontSize:16, marginBottom:dim[1]/72, marginLeft:dim[0]/39}}>
                https://www.kmk.org/fileadmin/Dateien/veroeffentlichungen_beschluesse/1998/1998_06_20_FS_Geistige_Entwickl.pdf   
                </Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={()=>{Linking.openURL('https://content-select.com/de/portal/media/view/58b02f78-8de8-455c-9650-64a8b0dd2d03')}}>
                <Text style={{color:'#0080ff', textAlign:'left', textDecorationLine:'underline', fontSize:16, marginBottom:dim[1]/72, marginLeft:dim[0]/39}}>
                https://content-select.com/de/portal/media/view/58b02f78-8de8-455c-9650-64a8b0dd2d03   
                </Text>
                </TouchableOpacity>




                <Text style={{color:'#ff0055',fontWeight:'bold', textAlign:'left', fontSize:22, marginVertical:dim[1]/39}}>
                Autoren:
                </Text>
                <Text style={{color:'#0080ff', textAlign:'left', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/72}}>
                    Richard Werkes: 
                </Text>
                <TouchableOpacity onPress={()=>{Linking.openURL('mailto: richard.werkes@rwth-aachen.de')}}>
                <Text style={{color:'#000', textAlign:'left', fontSize:16, marginBottom:dim[1]/72, marginLeft:dim[0]/39}}>
                richard.werkes@rwth-aachen.de
                </Text>
                </TouchableOpacity>
                <Text style={{color:'#0080ff', textAlign:'left', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/96}}>
                    Robin Kreft: 
                </Text>
                <TouchableOpacity onPress={()=>{Linking.openURL('mailto: robin.kreft@rwth-aachen.de')}}>
                <Text style={{color:'#000', textAlign:'left', fontSize:16, marginBottom:dim[1]/72, marginLeft:dim[0]/39}}>
                robin.kreft@rwth-aachen.de
                </Text>
                </TouchableOpacity> 
            </View>
            )
        case "LK":
            return(
                <View style={{width:dim[0]*.9}}>
                    <Text style={{color:'#ff0055',fontWeight:'bold', textAlign:'left', fontSize:22, marginVertical:dim[1]/39}}>
                        Kurzübersicht:
                    </Text>

                    <Text style={{color:'#000', textAlign:'justify', fontSize:16}}>
                    Der Förderbedarf liegt vor, wenn SuS in Ihrer Lern- und Leistungsentwicklung so beeinträchtigt sind, dass es zu einer hohen Diskrepanz zwischen den aktuellen Leistungsvoraussetzungen wie –möglichkeiten von SuS und der schulischen Leistungserwartung kommt. 
                    </Text>
                    <Text style={{color:'#000', textAlign:'justify', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/39}}>
                        Merkmale:
                    </Text>
                    <Text style={{color:'#0080ff', textAlign:'left', fontSize:16, marginBottom:dim[1]/72, marginLeft:dim[0]/39}}>
                        Schwerwiegend:
                        <Text style={{color:'#000'}}>
                        Große Unterschiede zwischen Ist (Leistungsvermögen)-und Ziel 	(Erwartung)- Zustand
                        </Text>
                    </Text>
                    <Text style={{color:'#0080ff', textAlign:'left', fontSize:16, marginVertical:dim[1]/72, marginLeft:dim[0]/39}}>
                        Umfänglich:
                        <Text style={{color:'#000'}}>
                            Allgemein, nicht auf ein Fach, einen Fachbereich beschränkt 
                        </Text>
                    </Text>
                    <Text style={{color:'#0080ff', textAlign:'left', fontSize:16,       marginVertical:dim[1]/72, marginLeft:dim[0]/39}}>
                        Langandauernd:
                        <Text style={{color:'#000'}}>
                            Dauert mehr als ein Jahr an
                        </Text>
                    </Text>
                </View>

            )
        case "LA":
            return(
                <View style={{width:dim[0]*.9}}>
                <Text style={{color:'#ff0055',fontWeight:'bold', textAlign:'left', fontSize:22, marginVertical:dim[1]/39}}>
                    Ausprägungen:
                </Text>
                <Text style={{color:'#0080ff', textAlign:'justify', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/72}}>
                    Eingeschränkte Lernvorrausetzungen
                </Text>
                <Text style={{color:'#000', textAlign:'left', fontSize:16, marginBottom:dim[1]/72, marginLeft:dim[0]/39}}>
                mangelnde Sprachkompetenz 
                </Text>
                <Text style={{color:'#000', textAlign:'left', fontSize:16, marginVertical:dim[1]/72, marginLeft:dim[0]/39}}>
                Einschränkungen im Arbeitsgedächtnis, in der Aufmerksamkeit, im Aufbau kognitiver Strukturen
                </Text>
                <Text style={{color:'#0080ff', textAlign:'left', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/72}}>
                Schwierigkeiten beim Denken, nutzen von Lernstrategien, bei Transferleistungen 
                </Text>
                <Text style={{color:'#0080ff', textAlign:'justify', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/72}}>
                Unzureichende Lernaktivitäten 
                </Text>
                <Text style={{color:'#000', textAlign:'left', fontSize:16, marginBottom:dim[1]/72, marginLeft:dim[0]/39}}>
                Unzureichende Eigensteuerung beim Lernen
                </Text>
                <Text style={{color:'#0080ff', textAlign:'left', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/72}}>
                Motivationsschwierigkeiten, geringes Aktivierungsniveau 
                </Text>
                <Text style={{color:'#0080ff', textAlign:'left', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/72}}>
                Auffälligkeiten im sozialen Handeln
                </Text>
                <Text style={{color:'#000', textAlign:'left', fontSize:16, marginBottom:dim[1]/72, marginLeft:dim[0]/39}}>
                bei Schuleintritt bis zu zwei Jahren Entwicklungsverzögerung 
                </Text>
                <Text style={{color:'#000', textAlign:'left', fontSize:16, marginBottom:dim[1]/72, marginLeft:dim[0]/39}}>
                hohe Wechselwirkung mit weiteren Förderschwerpunkten 
                </Text>
            </View>
            ) 
        case "LW":
            return(
                <View style={{width:dim[0]*.9}}>
                <Text style={{color:'#ff0055',fontWeight:'bold', textAlign:'left', fontSize:22, marginVertical:dim[1]/39}}>
                Was ist es nicht?
                </Text>
                <Text style={{color:'#0080ff', textAlign:'justify', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/72}}>
                    Isolierte Teilstörung 
                </Text>
                <Text style={{color:'#000', textAlign:'left', fontSize:16, marginBottom:dim[1]/72, marginLeft:dim[0]/39}}>
                Lese-Rechtschreibschwäche 
                </Text>
                <Text style={{color:'#000', textAlign:'left', fontSize:16, marginVertical:dim[1]/72, marginLeft:dim[0]/39}}>
                Schwäche im Rechnen 
                </Text>
                <Text style={{color:'#0080ff', textAlign:'left', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/72}}>
                „nur“ verringerte Intelligenz 
                </Text>
                <Text style={{color:'#0080ff', textAlign:'left', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/72}}>
                Temporärer Lernrückstand 
                </Text>
            </View>
            )
        case "LU":
            return(
                <View style={{width:dim[0]*.9}}>
                    <Text style={{color:'#ff0055',fontWeight:'bold', textAlign:'left', fontSize:22, marginVertical:dim[1]/39}}>
                    Ursachen:                    
                    </Text>
                    <Text style={{color:'#0080ff', textAlign:'justify', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/72}}>
                    Vielfältig                    
                    </Text>
                    <Text style={{color:'#0080ff', textAlign:'left', fontSize:16, fontWeight:'bold', marginBottom:dim[1]/72}}>
                    {'\u21D2'} Am häufigsten: 
                    </Text>
                    <Text style={{color:'#000', textAlign:'left', fontSize:16, marginVertical:dim[1]/72, marginLeft:dim[0]/39}}>
                    Erschwerte Lebenssituationen
                    </Text>
                    <Text style={{color:'#000', textAlign:'left', fontSize:16, marginVertical:dim[1]/72, marginLeft:dim[0]/39}}>
                    Psychische Erkrankungen 
                    </Text>
                    <Text style={{color:'#000', textAlign:'left', fontSize:16, marginVertical:dim[1]/72, marginLeft:dim[0]/39}}>
                    Entwicklungsstörungen                    </Text>
                    <Text style={{color:'#000', textAlign:'left', fontSize:16, marginVertical:dim[1]/72, marginLeft:dim[0]/39}}>
                    Verringerte Intelligenz (in Kombination)
                    </Text>
                    <Text style={{color:'#000', textAlign:'left', fontSize:16, marginVertical:dim[1]/72, marginLeft:dim[0]/39}}>
                    Körperliche Erkrankungen und Behinderungen
                    </Text>
                    <Text style={{color:'#0080ff', textAlign:'left', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/72}}>
                    {'\u2192'} personenverankert, institutionell erzeugt und/oder soziokulturell bedingt 
                    </Text>
                </View>
            )
        case "LT":
            return(
                <View style={{width:dim[0]*.9}}>
                <Text style={{color:'#ff0055',fontWeight:'bold', textAlign:'left', fontSize:22, marginVertical:dim[1]/39}}>
                Tipps:                    
                </Text>
                <Text style={{color:'#0080ff', textAlign:'left', fontSize:16, fontWeight:'bold', marginBottom:dim[1]/72}}>
                {'\u2705'} Förderplan der SuS einsehen 
                </Text>
                <Text style={{color:'#000', textAlign:'left', fontSize:16, marginVertical:dim[1]/72, marginLeft:dim[0]/39}}>
                Darstellung des Ist-Zustandes (Abschätzung der Ausprägung) 
                </Text>
                <Text style={{color:'#000', textAlign:'left', fontSize:16, marginVertical:dim[1]/72, marginLeft:dim[0]/39}}>
                Förderziel (Orientierung)
                </Text>
                <Text style={{color:'#0080ff', textAlign:'left', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/72}}>
                {'\u2705'} Mit der Klassen-Lehrkraft austauschen 
                </Text>
                <Text style={{color:'#0080ff', textAlign:'left', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/72}}>
                {'\u2705'} Mit evtl. 2. Lehrkraft absprechen (Co-Teaching) 
                </Text>
                <Text style={{color:'#0080ff', textAlign:'left', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/72}}>
                {'\u2705'} Etwaige individuelle Arbeitsaufträge recherchieren 
                </Text>
                <Text style={{color:'#0080ff', textAlign:'left', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/72}}>
                {'\u2705'} Im Klassenbuch bereits bekannte Methoden nachschlagen 
                </Text>
                <Text style={{color:'#0080ff', textAlign:'left', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/72}}>
                {'\u2705'} Mit der Unterstützung zunächst zurückhalten (erlernte Hilfslosigkeit)  
                </Text>
                <Text style={{color:'#0080ff', textAlign:'left', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/72}}>
                {'\u2705'} Strukturiertes, klares Vorgehen
                </Text>
                <Text style={{color:'#000', textAlign:'left', fontSize:16, marginVertical:dim[1]/72, marginLeft:dim[0]/39}}>
                Rituale erfragen und befolgen
                </Text>
                <Text style={{color:'#000', textAlign:'left', fontSize:16, marginVertical:dim[1]/72, marginLeft:dim[0]/39}}>
                Stundenthema und Ziel zu Beginn kommunizieren 
                </Text>
                <Text style={{color:'#000', textAlign:'left', fontSize:16, marginVertical:dim[1]/72, marginLeft:dim[0]/39}}>
                Klare Arbeitsaufträge, bei Bedarf mit Tafelsymbolen unterstützen 
                </Text>
                <Text style={{color:'#fff', textAlign:'left', fontSize:18, marginVertical:dim[1]/72,backgroundColor:'#0080ff',fontWeight:'bold' }}>
                Im Notfall: 
                <Text style={{fontWeight:'normal'}}>Einzel- und Kleingruppen Förderung</Text>
                </Text>
            </View>
            )
        case "LV":
            return(
                <View style={{width:dim[0]*.9}}>
                <Text style={{color:'#ff0055',fontWeight:'bold', textAlign:'left', fontSize:22, marginVertical:dim[1]/39}}>
                Besser Vermeiden:                    
                </Text>
                <Text style={{color:'#0080ff', textAlign:'left', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/72}}>
                {'\u274C'} Lehr-Lern Situationen, die die Schwächen von SuS konfrontieren 
                </Text>
                <Text style={{color:'#0080ff', textAlign:'left', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/72}}>
                {'\u274C'} Zu komplexe Sprache 
                </Text>
                <Text style={{color:'#0080ff', textAlign:'left', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/72}}>
                {'\u274C'} Unstrukturiertheit
                </Text>
            </View>
            )
        case "LB":
                return(
                    <View style={{width:dim[0]*.9}}>
                    <Text style={{color:'#ff0055',fontWeight:'bold', textAlign:'left', fontSize:22, marginVertical:dim[1]/39}}>
                    Berücksichtigung im Unterricht:                  
                    </Text>
                    <Text style={{color:'#000', textAlign:'justify', fontSize:16, fontWeight:'bold', marginTop:dim[1]/72}}>
                    Es gelten im Allgemeinen die Merkmale guten Unterrichts:                    
                    </Text>
                    <Text style={{color:'#000', textAlign:'justify', fontSize:16, fontWeight:'bold', marginBottom:dim[1]/72}}>
                    Drittelung nach Hilbert Meyer {'\u2192'} besonders effektiv:                     
                    </Text>
                    <Text style={{color:'#0080ff', textAlign:'left', fontSize:16, fontWeight:'bold', marginBottom:dim[1]/72}}>
                    Individualisierender Unterricht 
                    </Text>
                    <Text style={{color:'#000', textAlign:'left', fontSize:16, marginVertical:dim[1]/72, marginLeft:dim[0]/39}}>
                    Betreffende SuS müssen Eigenarbeit erst lernen, daran herangeführt werden 
                    </Text>
                    <Text style={{color:'#000', textAlign:'left', fontSize:16, marginVertical:dim[1]/72, marginLeft:dim[0]/39}}>
                    Wenige systematisch und regelmäßig trainierte Methoden 
                    </Text>
                    <Text style={{color:'#000', textAlign:'left', fontSize:16, marginVertical:dim[1]/72, marginLeft:dim[0]/39}}>
                    <Text style={{fontStyle:'italic'}}>Erlernte Hilfslosigkeit</Text> berücksichtigen (zu starke, vorauseilende Betreuung stärkt das Rückzugsverhalten, Hilfe wird angenommen statt die Aufgabe zu versuchen) 
                    </Text>
                    <Text style={{color:'#0080ff', textAlign:'left', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/72}}>
                    Direkte Konstruktion 
                    </Text>
                    <Text style={{color:'#000', textAlign:'left', fontSize:16, marginVertical:dim[1]/72, marginLeft:dim[0]/39}}>
                    Anteil lehrkraftzentrierter Unterrichtsformen 
                    </Text>
                    <Text style={{color:'#000', textAlign:'left', fontSize:16, marginVertical:dim[1]/72, marginLeft:dim[0]/39}}>
                    Üben unter stetig abnehmender Anleitung 
                    </Text>
                    <Text style={{color:'#000', textAlign:'left', fontSize:16, marginVertical:dim[1]/72, marginLeft:dim[0]/39}}>
                    Individualcoaching                    </Text>
                    <Text style={{color:'#000', textAlign:'left', fontSize:16, marginVertical:dim[1]/72, marginLeft:dim[0]/39}}>
                    Co-Teaching
                    </Text>
                    <Text style={{color:'#0080ff', textAlign:'left', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/72}}>
                    Kooperative Unterrichtsformen 
                    </Text>
                    <Text style={{color:'#000', textAlign:'left', fontSize:16, marginVertical:dim[1]/72, marginLeft:dim[0]/39}}>
                    Peergestütztes Lernen 
                    </Text>
                    <Text style={{color:'#000', textAlign:'left', fontSize:16, marginVertical:dim[1]/72, marginLeft:dim[0]/39}}>
                    Klassenweites Tutorensystem 
                    </Text>
                </View>
                )
        case "EK":
            return(
                <View style={{width:dim[0]*.9}}>
                    <Text style={{color:'#ff0055',fontWeight:'bold', textAlign:'left', fontSize:22, marginVertical:dim[1]/39}}>
                        Kurzübersicht:
                    </Text>

                    <Text style={{color:'#000', textAlign:'justify', fontSize:16}}>
                    Der Förderschwerpunkt Emotionale und soziale Entwicklung äußert sich meist durch auffälliges Verhalten von SuS bei gefühltem Kontrollverlust oder durch Abkapselung. Beobachtbar sind oft resultierende Verstöße gegen Regeln oder Autoritäten. 
                    Der Ursprung ist oft in der Lebenswelt der SuS, beispielsweise im familiären Umfeld.
                    </Text>
                    <Text style={{color:'#000', textAlign:'justify', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/39}}>
                    Zur Einstufung als Förderbedarf müssen folgende Punkte erfüllt sein:
                    </Text>
                    <Text style={{color:'#0080ff', textAlign:'left', fontSize:16, marginBottom:dim[1]/72, marginLeft:dim[0]/39}}>
                        Intensität: {'\u0020'}
                        <Text style={{color:'#000'}}>
                        Dauer und Schwere der Symptome
                        </Text>
                    </Text>
                    <Text style={{color:'#0080ff', textAlign:'left', fontSize:16, marginVertical:dim[1]/72, marginLeft:dim[0]/39}}>
                        Ökologie:{'\u0020'} 
                        <Text style={{color:'#000'}}>
                        Auftreten in Schule und anderem Umfeld
                        </Text>
                    </Text>
                    <Text style={{color:'#0080ff', textAlign:'left', fontSize:16, marginVertical:dim[1]/72, marginLeft:dim[0]/39}}>
                        Integration:{'\u0020'} 
                        <Text style={{color:'#000'}}>
                        Erforderlichkeit von Unterstützung für gesellschaftliche Eingliederung
                        </Text>
                    </Text>
                </View>
            )
        case "EA":
            return(
                <View style={{width:dim[0]*.9}}>
                    <Text style={{color:'#ff0055',fontWeight:'bold', textAlign:'left', fontSize:22, marginVertical:dim[1]/39}}>
                        Ausprägungen:
                    </Text>
                    <Text style={{color:'#0080ff', textAlign:'justify', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/72}}>
                    Kontrollverlust im Umgang mit Autoritäten
                    </Text>
                    <Text style={{color:'#000', textAlign:'left', fontSize:16, marginBottom:dim[1]/72, marginLeft:dim[0]/39}}>
                    Positiv wie negativ, Verlust der Kontrolle über eigene Emotionen
                    </Text>
                    <Text style={{color:'#0080ff', textAlign:'left', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/72}}>
                    Auffällige Verhaltensweisen
                    </Text>
                    <Text style={{color:'#000', textAlign:'left', fontSize:16, marginBottom:dim[1]/72, marginLeft:dim[0]/39}}>
                    Gewalttätigkeit, Gefährdung von sich und anderen
                    </Text>
                    <Text style={{color:'#0080ff', textAlign:'justify', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/72}}>
                    Verweigerung der Mitarbeit im Unterricht
                    </Text>
                    <Text style={{color:'#0080ff', textAlign:'left', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/72}}>
                    Selbstisolation: Verweigerung von Kommunikation etc.
                    </Text>
                    <Text style={{color:'#0080ff', textAlign:'left', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/72}}>
                    Stark sexualisiertes Verhalten und/oder Sprache
                    </Text>
                    <Text style={{color:'#0080ff', textAlign:'left', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/72}}>
                    Kriminelle Tätigkeiten
                    </Text>
                    <Text style={{color:'#000', textAlign:'left', fontSize:16, marginBottom:dim[1]/72, marginLeft:dim[0]/39}}>
                    Diebstahl, körperliche Übergriffe
                    </Text>
                </View> 
            ) 
        case "EW":
            return (
                <View style={{width:dim[0]*.9}}>
                    <Text style={{color:'#ff0055',fontWeight:'bold', textAlign:'left', fontSize:22, marginVertical:dim[1]/39}}>
                    Was ist es nicht?
                    </Text>
                    <Text style={{color:'#0080ff', textAlign:'justify', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/72}}>
                    Pauschales Stören im Unterricht                    </Text>
                    <Text style={{color:'#0080ff', textAlign:'left', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/72}}>
                    Situative, einmalige, emotionale Ausbrüche                    </Text>
                    <Text style={{color:'#0080ff', textAlign:'left', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/72}}>
                    Provokantes Verhalten in der Pubertät oder früheren Entwicklungsphasen
                    </Text>
                </View> 
            )
        case "EU":
            return(
                <View style={{width:dim[0]*.9}}>
                    <Text style={{color:'#ff0055',fontWeight:'bold', textAlign:'left', fontSize:22, marginVertical:dim[1]/39}}>
                    Ursachen:                        
                    </Text>
                    <Text style={{color:'#0080ff', textAlign:'justify', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/72}}>
                    Ausbildung in Interaktion mit sozialem Umfeld
                    </Text>
                    <Text style={{color:'#000', textAlign:'left', fontSize:16, marginBottom:dim[1]/72, marginLeft:dim[0]/39}}>
                    Schulisch, familiär, gesellschaftlich, persönlich
                    </Text>
                    <Text style={{color:'#0080ff', textAlign:'left', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/72}}>
                    Falsche Fremd- und Selbstwahrnehmung durch widersprüchliche Erfahrungen
                    </Text>
                    <Text style={{color:'#0080ff', textAlign:'left', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/72}}>
                    Einsamkeit, Hilflosigkeit, soziale Ausgrenzung (durch Armut o.Ä.)
                    </Text>
                    <Text style={{color:'#0080ff', textAlign:'left', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/72}}>
                    Emotionale Überforderung, Missbrauch
                    </Text>
                </View>   
            )
        case "ET":
            return(
                <View style={{width:dim[0]*.9}}>
                    <Text style={{color:'#ff0055',fontWeight:'bold', textAlign:'left', fontSize:22, marginVertical:dim[1]/39}}>
                    Tipps:                    
                    </Text>
                    <Text style={{color:'#0080ff', textAlign:'left', fontSize:16, fontWeight:'bold', marginBottom:dim[1]/72}}>
                    {'\u2705'} Kommunikation mit Familie und anderen Lehrkräften 
                    </Text>
                    <Text style={{color:'#0080ff', textAlign:'left', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/72}}>
                    {'\u2705'} Klassenrat als Umsetzung selbstbestimmter, demokratischer Umgangsformen 
                    </Text>
                    <Text style={{color:'#0080ff', textAlign:'left', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/72}}>
                    {'\u2705'} Angebote zur Prävention schaffen:
                    </Text>
                    <Text style={{color:'#000', textAlign:'left', fontSize:16, marginVertical:dim[1]/72, marginLeft:dim[0]/39}}>
                        Schulweit
                    </Text>
                    <Text style={{color:'#000', textAlign:'left', fontSize:16, marginVertical:dim[1]/72, marginLeft:dim[0]/39}}>
                        Sensibilisierung für Mobbing, Aggressionen, Ängste 
                    </Text>
                    <Text style={{color:'#0080ff', textAlign:'left', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/72}}>
                    {'\u2705'} Einbezug von Sozialarbeiter*innen für Kontextübergreifende Arbeit
                    </Text>
                    <Text style={{color:'#0080ff', textAlign:'left', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/72}}>
                    {'\u2705'} Individuelle Fallbetrachtung 
                    </Text>
                    <Text style={{color:'#000', textAlign:'left', fontSize:16, marginVertical:dim[1]/72, marginLeft:dim[0]/39}}>
                    Überschneidungen mit anderen Schwerpunkten prüfen (‚Underachiever‘)                    </Text>
                    <Text style={{color:'#0080ff', textAlign:'left', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/72}}>
                    {'\u2705'} Beratungsangebote für Lehrkräfte wahrnehmen  
                    </Text>
                </View>
            )
        case "EV":
            return(
                <View style={{width:dim[0]*.9}}>
                    <Text style={{color:'#ff0055',fontWeight:'bold', textAlign:'left', fontSize:22, marginVertical:dim[1]/39}}>
                    Besser Vermeiden:                    
                    </Text>
                    <Text style={{color:'#0080ff', textAlign:'left', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/72}}>
                    {'\u274C'} Stresssituationen durch direkte/öffentliche Konfrontation  
                    </Text>
                    <Text style={{color:'#0080ff', textAlign:'left', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/72}}>
                    {'\u274C'} Intransparente/unklare Aussagen (auch Benotung, Feedback)
                    </Text>
                    <Text style={{color:'#0080ff', textAlign:'left', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/72}}>
                    {'\u274C'} Improvisierter Unterricht, Leerlaufphasen
                    </Text>
                </View>
            )
        case "EB":
            return(
                <View style={{width:dim[0]*.9}}>
                    <Text style={{color:'#ff0055',fontWeight:'bold', textAlign:'left', fontSize:22, marginVertical:dim[1]/39}}>
                    Berücksichtigung im Unterricht:                  
                    </Text>
                    <Text style={{color:'#000', textAlign:'justify', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/72}}>
                    Das Schaffen von kontrollierten, organisierten Unterrichtsumfeldern steht im Kern der inklu-{'\u0020'}siven Förderung: 
                    </Text>
                    <Text style={{color:'#0080ff', textAlign:'left', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/72}}>
                    Classroom Management
                    </Text>
                    <Text style={{color:'#000', textAlign:'left', fontSize:16, marginBottom:dim[1]/72, marginLeft:dim[0]/39}}>
                    Organisierter Klassenraum, klare Struktur im Unterricht, Transparenz
                    </Text>
                    <Text style={{color:'#0080ff', textAlign:'justify', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/72}}>
                    Ressourcenorientierte Förderung
                    </Text>
                    <Text style={{color:'#000', textAlign:'left', fontSize:16, marginBottom:dim[1]/72, marginLeft:dim[0]/39}}>
                    Aufbau sozialer Kompetenzen; Remediation (Aufbau von Lernvoraussetzungen): Selbständigkeit im Vordergrund: Soziale Strukturen schaffen, um Miteinander und Vertrauen zu vermitteln; (sozial-)kooperative Unterrichtsformen
                    </Text>
                    <Text style={{color:'#0080ff', textAlign:'left', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/72}}>
                    Störungsspezifische Förderung
                    </Text>
                    <Text style={{color:'#000', textAlign:'left', fontSize:16, marginBottom:dim[1]/72, marginLeft:dim[0]/39}}>
                    Diagnose und Abbau von Problemstellen im Unterrichtskontext: Lehrkraft muss auch eigene subjektive Wahrnehmung hinterfragen
                    </Text>
                    <Text style={{color:'#0080ff', textAlign:'left', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/72}}>
                    Fokus auf Differenzierung, wenn nötig
                    </Text>
                    <Text style={{color:'#000', textAlign:'left', fontSize:16, marginBottom:dim[1]/72, marginLeft:dim[0]/39}}>
                    Erfahrungswelt als Anknüpfungspunkt für Themen; Waage zwischen individuellen Voraussetzungen und Angeboten
                    </Text>
                </View>    
            )
        case "GK":
            return(
                <View style={{width:dim[0]*.9}}>
                    <Text style={{color:'#ff0055',fontWeight:'bold', textAlign:'left', fontSize:22, marginVertical:dim[1]/39}}>
                        Kurzübersicht:
                    </Text>
                    <Text style={{color:'#000', textAlign:'justify', fontSize:16}}>
                    Der Förderschwerpunkt geistige Entwicklung liegt vor, wenn SuS im Bereich der kognitiven Funktion massiv eingeschränkt sind. Neben dem schulischen Lernen ist die Entwicklung der Gesamtpersönlichkeit dauerhaft hochgradig beeinträchtigt und es kann sein, dass die SuS für eine selbständige Lebensführung auch nach Ende der Schulzeit dauerhaft Hilfe benötigen.
                    </Text>
                    <Text style={{color:'#000', textAlign:'justify', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/39}}>
                    Merkmale:                    
                    </Text>
                    <Text style={{color:'#0080ff', textAlign:'left', fontSize:16, marginBottom:dim[1]/72, marginLeft:dim[0]/39}}>
                    Sammelbegriff: {'\u0020'}
                        <Text style={{color:'#000'}}>
                        lebenslange Einschränkungen in kognitiven und (meistens auch) sozialen Bereichen
                        </Text>
                    </Text>
                    <Text style={{color:'#0080ff', textAlign:'left', fontSize:16, marginVertical:dim[1]/72, marginLeft:dim[0]/39}}>
                    Verschiedenste Äußerungsformen:{'\u0020'} 
                        <Text style={{color:'#000'}}>
                        Meist weitere Entwicklungsbereiche betroffen 
                        </Text>
                    </Text>
                    <Text style={{color:'#0080ff', textAlign:'left', fontSize:16, marginVertical:dim[1]/72, marginLeft:dim[0]/39}}>
                    Nur selten SuS auf Regelschulen
                    </Text>
                </View>
            )
        case "GA":
            return(
                <View style={{width:dim[0]*.9}}>
                    <Text style={{color:'#ff0055',fontWeight:'bold', textAlign:'left', fontSize:22, marginVertical:dim[1]/39}}>
                        Ausprägungen:
                    </Text>
                    <Text style={{color:'#0080ff', textAlign:'justify', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/72}}>
                    Unterdurchschnittliche kognitive Fähigkeiten 
                    </Text>
                    <Text style={{color:'#0080ff', textAlign:'justify', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/72}}>
                    Äußerst diverse Erscheinungsbilder, insbesondere Auswirkungen auf: 
                    </Text>
                    <Text style={{color:'#000', textAlign:'left', fontSize:16, marginBottom:dim[1]/72, marginLeft:dim[0]/39}}>
                    Das Lernen 
                    </Text>
                    <Text style={{color:'#000', textAlign:'left', fontSize:16, marginBottom:dim[1]/72, marginLeft:dim[0]/39}}>
                    Selbstgesteuerte Aktivitäten 
                    </Text>
                    <Text style={{color:'#000', textAlign:'left', fontSize:16, marginBottom:dim[1]/72, marginLeft:dim[0]/39}}>
                    Gedächtnisleistung 
                    </Text>
                    <Text style={{color:'#000', textAlign:'left', fontSize:16, marginBottom:dim[1]/72, marginLeft:dim[0]/39}}>
                    Kommunikationsfähigkeit
                    </Text>                    
                    <Text style={{color:'#000', textAlign:'left', fontSize:16, marginBottom:dim[1]/72, marginLeft:dim[0]/39}}>
                    Selbstkontrolle 
                    </Text>                    
                    <Text style={{color:'#000', textAlign:'left', fontSize:16, marginBottom:dim[1]/72, marginLeft:dim[0]/39}}>
                    Selbstbewusstsein 
                    </Text>
                    <Text style={{color:'#000', textAlign:'left', fontSize:16, marginBottom:dim[1]/72, marginLeft:dim[0]/39}}>
                    ...                    
                    </Text>
                    <Text style={{color:'#0080ff', textAlign:'left', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/72}}>
                    Weitere Entwicklungsbereiche 
                    </Text>
                    <Text style={{color:'#0080ff', textAlign:'left', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/72}}>
                    Unzureichende Lernaktivitäten 
                    </Text>
                    <Text style={{color:'#000', textAlign:'left', fontSize:16, marginBottom:dim[1]/72, marginLeft:dim[0]/39}}>
                    Wahrnehmung 
                    </Text>
                    <Text style={{color:'#000', textAlign:'left', fontSize:16, marginBottom:dim[1]/72, marginLeft:dim[0]/39}}>
                    Wahrnehmung 
                    </Text>                    
                    <Text style={{color:'#000', textAlign:'left', fontSize:16, marginBottom:dim[1]/72, marginLeft:dim[0]/39}}>
                    Motorik  
                    </Text>                    
                    <Text style={{color:'#000', textAlign:'left', fontSize:16, marginBottom:dim[1]/72, marginLeft:dim[0]/39}}>
                    Aufmerksamkeit 
                    </Text>
                    <Text style={{color:'#000', textAlign:'left', fontSize:16, marginBottom:dim[1]/72, marginLeft:dim[0]/39}}>
                    Sozial-emotionaler Bereich  
                    </Text>
                    <Text style={{color:'#000', textAlign:'left', fontSize:16, marginBottom:dim[1]/72, marginLeft:dim[0]/39}}>
                    … 
                    </Text>
                    <Text style={{color:'#0080ff', textAlign:'justify', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/72}}>
                    Motivationsschwierigkeiten, geringes Aktivierungsniveau. 
                    </Text>
                    <Text style={{color:'#0080ff', textAlign:'left', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/72}}>
                    Auffälligkeiten im sozialen Handeln
                    </Text>
                    <Text style={{color:'#000', textAlign:'left', fontSize:16, marginBottom:dim[1]/72, marginLeft:dim[0]/39}}>
                    bei Schuleintritt bis zu zwei Jahren Entwicklungsverzögerung 
                    </Text>
                    <Text style={{color:'#000', textAlign:'left', fontSize:16, marginBottom:dim[1]/72, marginLeft:dim[0]/39}}>
                    hohe Wechselwirkung mit weiteren Förderschwerpunkten 
                    </Text>
                </View> 
            )
        case "GW":
            return(
                <View style={{width:dim[0]*.9}}>
                    <Text style={{color:'#ff0055',fontWeight:'bold', textAlign:'left', fontSize:22, marginVertical:dim[1]/39}}>
                    Was ist es nicht?
                    </Text>
                    <Text style={{color:'#0080ff', textAlign:'justify', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/72}}>
                    „nur“ starke Lernstörungen
                    </Text> 
                    <Text style={{color:'#0080ff', textAlign:'left', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/72}}>
                    Vorübergehende Auswirkungen einer Krankheit auf die kognitive Funktion 
                    </Text>
                </View> 
            )
        case "GU":
            return(
                <View style={{width:dim[0]*.9}}>
                <Text style={{color:'#ff0055',fontWeight:'bold', textAlign:'left', fontSize:22, marginVertical:dim[1]/39}}>
                    Ursachen:
                </Text>
                <Text style={{color:'#000', textAlign:'justify', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/39}}>
                Nur bei 40 – 60% ermittelbar                    
                </Text>
                <Text style={{color:'#0080ff', textAlign:'left', fontSize:16, marginBottom:dim[1]/72, marginLeft:dim[0]/39}}>
                Chromosal: {'\u0020'}
                    <Text style={{color:'#000'}}>
                    verursachte geistige Behinderung                    
                    </Text>
                </Text>
                <Text style={{color:'#000', textAlign:'left', fontSize:16, marginBottom:dim[1]/72, marginLeft:dim[0]/39}}>
                Veränderung in Anzahl und Struktur der Chromosomen 
                </Text>
                <Text style={{color:'#000', textAlign:'left', fontSize:16, marginBottom:dim[1]/72, marginLeft:dim[0]/39}}>
                Bsp.: Down-; Katzenschrei –Syndrom
                </Text>
                <Text style={{color:'#0080ff', textAlign:'left', fontSize:16, marginVertical:dim[1]/72, marginLeft:dim[0]/39}}>
                    Metabolisch:{'\u0020'} 
                    <Text style={{color:'#000'}}>
                    verursachte geistige Behinderung                    
                    </Text>
                </Text>
                <Text style={{color:'#000', textAlign:'left', fontSize:16, marginBottom:dim[1]/72, marginLeft:dim[0]/39}}>
                Stoffwechselstörungen ursächlich 
                </Text>
                <Text style={{color:'#000', textAlign:'left', fontSize:16, marginBottom:dim[1]/72, marginLeft:dim[0]/39}}>
                Hirnschädigungen durch nicht-Abbau von Giftstoffen 
                </Text>
                <Text style={{color:'#0080ff', textAlign:'left', fontSize:16, marginVertical:dim[1]/72, marginLeft:dim[0]/39}}>
                    Exogen:{'\u0020'} 
                    <Text style={{color:'#000'}}>
                    verursachte geistige Behinderung                   
                    </Text>
                </Text>
                <Text style={{color:'#000', textAlign:'left', fontSize:16, marginBottom:dim[1]/72, marginLeft:dim[0]/39}}>
                Einwirkung von außen: Geburtskomplikationen, Erkrankungen der Mutter, Rauschmittel in der Schwangerschaft usw. 
                </Text>
            </View>
            )
        case "GT":
            return(
                <View style={{width:dim[0]*.9}}>
                    <Text style={{color:'#ff0055',fontWeight:'bold', textAlign:'left', fontSize:22, marginVertical:dim[1]/39}}>
                    Tipps:                    
                    </Text>
                    <Text style={{color:'#000', textAlign:'justify', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/39}}>
                    Allgemein                    
                    </Text>
                    <Text style={{color:'#0080ff', textAlign:'left', fontSize:16, fontWeight:'bold', marginBottom:dim[1]/72}}>
                    {'\u2705'} Individuellen Entwicklungstand der SuS möglichst differenziert einschätzen   
                    </Text>
                    <Text style={{color:'#000', textAlign:'left', fontSize:16, marginBottom:dim[1]/72, marginLeft:dim[0]/39}}>
                    Individuelle Ausgangslage ermit-teln, entsprechende Förderung 
                    </Text>
                    <Text style={{color:'#000', textAlign:'left', fontSize:16, marginBottom:dim[1]/72, marginLeft:dim[0]/39}}>
                    Individuelle Lerhpläne aus bestehenden ableiten 
                    </Text>
                    <Text style={{color:'#000', textAlign:'left', fontSize:16, marginBottom:dim[1]/72, marginLeft:dim[0]/39}}>
                    Weitere betroffene Bereiche identifizieren und Verwandte Förder-schwerpunkte recherchieren  
                    </Text>
                    <Text style={{color:'#0080ff', textAlign:'left', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/72}}>
                    {'\u2705'} Klassenrat als Umsetzung selbstbestimmter, demokratischer Umgangsformen 
                    </Text>
                    <Text style={{color:'#0080ff', textAlign:'left', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/72}}>
                    {'\u2705'} Einzel-/Kleingruppenförderung für Inhalte
                    </Text>
                    <Text style={{color:'#0080ff', textAlign:'left', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/72}}>
                    {'\u2705'}  Gemeinsamer Unterricht zum erlangen gesellschaftlicher Fähigkeiten 
                    </Text>
                    <Text style={{color:'#000', textAlign:'justify', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/39}}>
                    In Vertretungssituationen                     
                    </Text>
                    <Text style={{color:'#0080ff', textAlign:'left', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/72}}>
                    {'\u2705'} Förderplan und Lehrplan der SuS einsehen  
                    </Text>
                    <Text style={{color:'#000', textAlign:'left', fontSize:16, marginVertical:dim[1]/72, marginLeft:dim[0]/39}}>
                    Individuelles Schulziel ermitteln                   
                    </Text>
                    <Text style={{color:'#000', textAlign:'left', fontSize:16, marginVertical:dim[1]/72, marginLeft:dim[0]/39}}>
                    Ausprägung ermitteln: Weitere Förderbereiche identifizieren und beachten  
                    </Text>
                    <Text style={{color:'#0080ff', textAlign:'left', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/72}}>
                    {'\u2705'} Mit der Klassen-Lehrkraft austauschen 
                    </Text>
                    <Text style={{color:'#0080ff', textAlign:'left', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/72}}>
                    {'\u2705'} Mit evtl. 2. Lehrkraft, Sozialarbeiter absprechen 
                    </Text>
                    <Text style={{color:'#0080ff', textAlign:'left', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/72}}>
                    {'\u2705'} Evtl. besondere Fördermethoden (bspw. Bewegungsförderung) ermitteln  
                    </Text>
                </View>
            )
        case "GV":
            return(
                <View style={{width:dim[0]*.9}}>
                <Text style={{color:'#ff0055',fontWeight:'bold', textAlign:'left', fontSize:22, marginVertical:dim[1]/39}}>
                Besser Vermeiden:                    
                </Text>
                <Text style={{color:'#0080ff', textAlign:'left', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/72}}>
                {'\u274C'} Komplexität (Inhalt, Methoden)
                </Text>
                <Text style={{color:'#0080ff', textAlign:'left', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/72}}>
                {'\u274C'} Beschulung ohne Anpassung an die individuelle Ausprägung
                </Text>
                <Text style={{color:'#0080ff', textAlign:'left', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/72}}>
                {'\u274C'} Selbstorganisierte Arbeitsphasen
                </Text>

            </View>
            )
        case "GB":
            return(
                <View style={{width:dim[0]*.9}}>
                    <Text style={{color:'#ff0055',fontWeight:'bold', textAlign:'left', fontSize:22, marginVertical:dim[1]/39}}>
                    Berücksichtigung im Unterricht:                  
                    </Text>
                    <Text style={{color:'#000', textAlign:'justify', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/72}}>
                    SuS werden zu einem eigenen Abschlüssen geführt: Zieldifferenter Unterricht 
                    </Text>
                    <Text style={{color:'#000', textAlign:'left', fontSize:16, marginBottom:dim[1]/72, marginLeft:dim[0]/39}}>
                    Individuelles, auf das spätere (Berufs)leben ausgerichtetes Schulziel
                    </Text>
                    <Text style={{color:'#0080ff', textAlign:'left', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/72}}>
                    Der Unterricht ist auf die starke Heterogenität der SuS mit Blick auf das Schulziel ausgelegt
                    </Text>
                    <Text style={{color:'#000', textAlign:'left', fontSize:16, marginBottom:dim[1]/72, marginLeft:dim[0]/39}}>
                    Berücksichtigung der individuellen Ausprägung, des aktuellen Entwicklungsstands 
                    </Text>
                    <Text style={{color:'#000', textAlign:'left', fontSize:16, marginBottom:dim[1]/72, marginLeft:dim[0]/39}}>
                    Orientierung an den Prinzipien guten Unterrichts 
                    </Text>
                    <Text style={{color:'#000', textAlign:'justify', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/72}}>
                    Elementar für den Unterricht im Förderschwerpunkt geistige Entwicklung: 
                    </Text>
                    <Text style={{color:'#0080ff', textAlign:'justify', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/72}}>
                    Didaktische Reduktion & Strukturierung 
                    </Text>
                    <Text style={{color:'#000', textAlign:'left', fontSize:16, marginBottom:dim[1]/72, marginLeft:dim[0]/39}}>
                    Sehr starke Reduktion und Elementarisierung der Inhalte (möglichst geringe Komplexität)
                    </Text>
                    <Text style={{color:'#000', textAlign:'left', fontSize:16, marginBottom:dim[1]/72, marginLeft:dim[0]/39}}>
                    Kleinschrittige Planung kleiner, gut aufeinander abgestimmter Lerneinheiten  
                    </Text>
                    <Text style={{color:'#0080ff', textAlign:'left', fontSize:16, fontWeight:'bold', marginVertical:dim[1]/72}}>
                    Handlungsorientierung & bedeutende Kontexte 
                    </Text>
                    <Text style={{color:'#000', textAlign:'left', fontSize:16, marginBottom:dim[1]/72, marginLeft:dim[0]/39}}>
                    Einbettung der Inhalte in bedeutende, lebensnahe Kontexte und Handlungen
                    </Text>
                    <Text style={{color:'#000', textAlign:'left', fontSize:16, marginBottom:dim[1]/72, marginLeft:dim[0]/39}}>
                    Methoden am Entwicklungsstand und den Fähigkeiten ausrichten
                    </Text>
                    <Text style={{color:'#000', textAlign:'left', fontSize:16, marginBottom:dim[1]/72, marginLeft:dim[0]/39}}>
                    An den Fähigkeiten und Neigungen orientieren, Stärken herausstellen: (bspw. hohe soziale Kompetenz bei SuS mit Down-Syndrom nutzen) 
                    </Text>
                </View>    
            )
        default:
            break;
    }

}

export const get_tts=(param)=>{
    switch (param) {
        case "about":
            return(
                "Die InkluInfo App wurde von Richard Werkes und Robin Kreft entworfen. Unter Quellen ist die genutzte Literatur zu allen Förderschwerpunkten aufgelistet."
            )
        case "LK":
            return(
    	        "Kurzübersicht: Der Förderbedarf liegt vor, wenn SuS in Ihrer Lern- und Leistungsentwicklung so beeinträchtigt sind, dass es zu einer hohen Diskrepanz zwischen den aktuellen Leistungsvoraussetzungen wie –möglichkeiten von SuS und der schulischen Leistungserwartung kommt. Merkmale: Schwerwiegend:Große Unterschiede zwischen Ist (Leistungsvermögen)-und Ziel (Erwartung)- ZustandUmfänglich: Allgemein, nicht auf ein Fach, einen Fachbereich beschränkt Langandauernd: Dauert mehr als ein Jahr an"
            )
        case "LA":
            return(
                    "Ausprägungen: Eingeschränkte Lernvorrausetzungen: mangelnde Sprachkompetenz, Einschränkungen im Arbeitsgedächtnis, in der Aufmerksamkeit, im Aufbau kognitiver Strukturen. Schwierigkeiten beim Denken, nutzen von Lernstrategien, bei Transferleistungen. Unzureichende Lernaktivitäten: Unzureichende Eigensteuerung beim Lernen. Motivationsschwierigkeiten, geringes Aktivierungsniveau. Auffälligkeiten im sozialen Handeln: bei Schuleintritt bis zu zwei Jahren Entwicklungsverzögerung, hohe Wechselwirkung mit weiteren Förderschwerpunkten "
            ) 
        case "LW":
            return(
                    "Was ist es nicht? Isolierte Teilstörung. Lese-Rechtschreibschwäche, Schwäche im Rechnen. „nur“ verringerte Intelligenz. Temporärer Lernrückstand"
            )
        case "LU":
                return(
                    "Ursachen: Vielfältig: Am häufigsten: Erschwerte Lebenssituationen, Psychische Erkrankungen, Entwicklungsstörungen, Verringerte Intelligenz (in Kombination), Körperliche Erkrankungen und Behinderungen. Personenverankert, institutionell erzeugt und/oder soziokulturell bedingt"
                )
        case "LT":
                return(
                	"Tipps: Förderplan der SuS einsehen: Darstellung des Ist-Zustandes  (Abschätzung der Ausprägung), Förderziel (Orientierung). Mit der Klassen-Lehrkraft austauschen. Mit evtl. 2. Lehrkraft absprechen (Co-Teaching). Etwaige individuelle Arbeitsaufträge recherchieren. Im Klassenbuch bereits bekannte Methoden nachschlagen.Mit der Unterstützung zunächst zurückhalten (erlernte Hilfslosigkeit). Strukturiertes, klares Vorgehen: Rituale erfragen und befolgen, Stundenthema und Ziel zu Beginn kommunizieren, Klare Arbeitsaufträge, bei Bedarf mit Tafelsymbolen unterstützen. Im Notfall: Einzel- und Kleingruppenförderung."
                )
        case "LV":
                return(
                    "Besser Vermeiden: Lehr-Lern Situationen, die die Schwächen von SuS konfrontieren. Zu komplexe Sprache. Unstrukturiertheit "
                )
        case "LB":
                return(
                    "Berücksichtigung im Unterricht: Es gelten im Allgemeinen die Merkmale guten Unterrichts: Drittelung nach Hilbert Meyer besonders effektiv: Individualisierender Unterricht: Betreffende SuS müssen Eigenarbeit erst lernen, daran herangeführt werden; Wenige systematisch und regelmäßig trainierte Methoden; Erlernte Hilfslosigkeit berücksichtigen (zu starke, vorauseilende Betreuung stärkt das Rückzugsverhalten, Hilfe wird angenommen statt die Aufgabe zu versuchen). Direkte Konstruktion: Anteil lehrkraftzentrierter Unterrichtsformen; Üben unter stetig abnehmender Anleitung;  Individualcoaching; Co-Teaching. Kooperative Unterrichtsformen: Peergestütztes Lernen; Klassenweites Tutorensystem. "
                )
        case "EK":
            return(
                "Kurzübersicht: Der Förderschwerpunkt Emotionale und soziale Entwicklung äußert sich meist durch auffälliges Verhalten von SuS bei gefühltem Kontrollverlust oder durch Abkapselung. Beobachtbar sind oft resultierende Verstöße gegen Regeln oder Autoritäten. Der Ursprung ist oft in der Lebenswelt der SuS, beispielsweise im familiären Umfeld. Zur Einstufung als Förderbedarf müssen folgende Punkte erfüllt sein: Intensität: Dauer und Schwere der Symptome. Ökologie: Auftreten in Schule und anderem Umfeld. Integration: Erforderlichkeit von Unterstützung für gesellschaftliche Eingliederung."
            )
        case "EA":
            return(
                "Ausprägungen: Kontrollverlust im Umgang mit Autoritäten: Positiv wie negativ, Verlust der Kontrolle über eigene Emotionen. Auffällige Verhaltensweisen: Gewalttätigkeit, Gefährdung von sich und anderen. Verweigerung der Mitarbeit im Unterricht. Selbstisolation: Verweigerung von Kommunikation etc.. Stark sexualisiertes Verhalten und/oder Sprache. Kriminelle Tätigkeiten: Diebstahl, körperliche Übergriffe."
            )
        case "EW":
            return (
                " Was ist es nicht? Pauschales Stören im Unterricht. Situative, einmalige, emotionale Ausbrüche. Provokantes Verhalten in der Pubertät oder früheren Entwicklungsphasen."
            )
        case "EU":
            return(
                "Ursachen: Ausbildung in Interaktion mit sozialem Umfeld: Schulisch, familiär, gesellschaftlich, persönlich. Falsche Fremd- und Selbstwahrnehmung durch widersprüchliche Erfahrungen. Einsamkeit, Hilflosigkeit, soziale Ausgrenzung (durch Armut o.Ä.). Emotionale Überforderung, Missbrauch."
            )
        case "ET":
            return(
                "Tipps: Kommunikation mit Familie und anderen Lehrkräften. Klassenrat als Umsetzung selbstbestimmter, demokratischer Umgangsformen. Angebote zur Prävention schaffen: Schulweit; Sensibilisierung für Mobbing, Aggressionen, Ängste. Einbezug von Sozialarbeiter*innen für Kontextübergreifende Arbeit. Individuelle Fallbetrachtung: Überschneidungen mit anderen Schwerpunkten prüfen (Underachiever). Beratungsangebote für Lehrkräfte wahrnehmen."
            )
        case "EV":
            return(
                "Besser Vermeiden: Stresssituationen durch direkte/öffentliche Konfrontation. Intransparente/unklare Aussagen (auch Benotung, Feedback). Improvisierter Unterricht, Leerlaufphasen."
            )
        case "EB":
            return(
                "Berücksichtigung im Unterricht: Das Schaffen von kontrollierten, organisierten Unterrichtsumfeldern steht im Kern der inklusiven Förderung: Classroom Management: Organisierter Klassenraum, klare Struktur im Unterricht, Transparenz. Ressourcenorientierte Förderung: Aufbau sozialer Kompetenzen; Remediation (Aufbau von Lernvoraussetzungen): Selbständigkeit im Vordergrund: Soziale Strukturen schaffen, um Miteinander und Vertrauen zu vermitteln; (sozial-)kooperative Unterrichtsformen. Störungsspezifische Förderung: Diagnose und Abbau von Problemstellen im Unterrichtskontext: Lehrkraft muss auch eigene subjektive Wahrnehmung hinterfragen. Fokus auf Differenzierung, wenn nötig: Erfahrungswelt als Anknüpfungspunkt für Themen; Waage zwischen individuellen Voraussetzungen und Angeboten:"
            )
        case "GK":
            return(
                "Kurzübersicht: Der Förderschwerpunkt geistige Entwicklung liegt vor, wenn SuS im Bereich der kognitiven Funktion massiv eingeschränkt sind. Neben dem schulischen Lernen ist die Entwicklung der Gesamtpersönlichkeit dauerhaft hochgradig beeinträchtigt und es kann sein, dass die SuS für eine selbständige Lebensführung auch nach Ende der Schulzeit dauerhaft Hilfe benötigen. Merkmale: Sammelbegriff: lebenslange Einschränkungen in kognitiven und (meistens auch) sozialen Bereichen. Verschiedenste Äußerungsformen: Meist weitere Entwicklungsbereiche betroffen. Nur selten SuS auf Regelschulen."
            )
        case "GA":
            return(
                "Ausprägungen: Unterdurchschnittliche kognitive Fähigkeiten. Äußerst diverse Erscheinungsbilder, insbesondere Auswirkungen auf: Das Lernen. Selbstgesteuerte Aktivitäten. Gedächtnisleistung. Kommunikationsfähigkeit. Selbstkontrolle. Selbstbewusstsein. Weitere Entwicklungsbereiche: Wahrnehmung. Motorik. Aufmerksamkeit. Sozial-emotionaler Bereich."
                )
        case "GW":
            return(
                "Was ist es nicht? nur starke Lernstörungen. Vorübergehende Auswirkungen einer Krankheit auf die kognitive Funktion."
            )
        case "GU":
            return(
                "Ursachen: Nur bei 40 bis 60% ermittelbar. Chromosal verursachte geistige Behinderung: Veränderung in Anzahl und Struktur der Chromosomen. Bsp.: Down-; Katzenschrei –Syndrom. Metabolisch verursachte geistige Behinderung: Stoffwechselstörungen ursächlich. Hirnschädigungen durch nicht-Abbau von Giftstoffen. Exogen verursachte geistige Behinderung: Einwirkung von außen Geburtskomplikationen, Erkrankungen der Mutter, Rauschmittel in der Schwangerschaft usw."
            )
        case "GT":
            return(
                "Tipps: Allgemein: Individuellen Entwicklungstand der SuS möglichst differenziert einschätzen. Individuelle Ausgangslage ermitteln, entsprechende Förderung. Individuelle Lehrpläne aus bestehenden ableiten. Weitere betroffene Bereiche identifizieren; Verwandte Förder-schwerpunkte recherchieren. Einzel-/Kleingruppenförderung für Inhalte.  Gemeinsamer Unterricht zum erlangen gesellschaftlicher Fähigkeiten. In Vertretungssituationen: Förderplan und Lehrplan der SuS einsehen. Individuelles Schulziel ermitteln. Ausprägung ermitteln. Weitere Förderbereiche identifizieren und beachten. Mit der Klassen-Lehrkraft austauschen. Mit evtl. 2. Lehrkraft, Sozialarbeiter absprechen. Evtl. besondere Fördermethoden (bspw. Bewegungsförderung) ermitteln."   
            )
        case "GV":
            return(
                "Besser Vermeiden: Komplexität (Inhalt, Methoden). Beschulung ohne Anpassung an die individuelle Ausprägung. Selbstorganisierte Arbeitsphasen."
            )
        case "GU":
            return(
                "Berücksichtigung im Unterricht: SuS werden zu einem eigenen Abschlüssen geführt: Zieldifferenter Unterricht. Individuelles, auf das spätere (Berufs)leben ausgerichtetes Schulziel. Der Unterricht ist auf die starke Heterogenität der SuS mit Blick auf das Schulziel ausgelegt. Berücksichtigung der individuellen Ausprägung, des aktuellen Entwicklungsstands. Orientierung an den Prinzipien guten Unterrichts. Elementar für den Unterricht im Förderschwerpunkt geistige Entwicklung: Didaktische Reduktion & Strukturierung: Sehr starke Reduktion und Elementarisierung der Inhalte (möglichst geringe Komplexität). Kleinschrittige Planung kleiner, gut aufeinander abgestimmter Lerneinheiten. Handlungsorientierung & bedeutende Kontexte: Einbettung der Inhalte in bedeutende, lebensnahe Kontexte und Handlungen. Methoden am Entwicklungsstand und den Fähigkeiten ausrichten. An den Fähigkeiten und Neigungen orientieren, Stärken herausstellen. (bspw. hohe soziale Kompetenz bei SuS mit Down-Syndrom nutzen)."
            )
        default:
            break;
    }
}
