import { get_data } from './data';

const data = get_data()

export const filter_data = (s_item, topic)=>{

    var params = [...new Set(s_item.split(" "))]
    var res = []
    
    if(params[0]!=""){
        params.forEach(s_param=>{
            data.forEach(element=>{
                if(element[2]==s_param){
                    if(topic.length>1){
                        if(topic==element[1]){
                            res.push([element[1],element[0],element[2],element[4]])
                        }
                    }else{
                        res.push([element[1],element[0],element[2],element[4]])
                    }

                }
                else{
                    element[3].forEach(deep_element=>{
                        if(deep_element==s_param){
                            if(topic.length>1){
                                if(topic==element[1]){
                                    res.push([element[1],element[0],element[2],element[4]])
                                }
                            }else{
                                res.push([element[1],element[0],element[2],element[4]])
                            }
                        }
                    })
                }
            })
        })
    }
    else{
        data.forEach(element=>{
            if(element[1]==topic){
                res.push([element[1],element[0],element[2],element[4]])
            }
        })
    }

    var res_obj = []
    for(var i=0;i<res.length;i++){
        res_obj.push(
            {'id': i.toString(),'title': res[i][0],'text': res[i][1], 'chapter': res[i][2],'color': res[i][3]}
        )
    }
    return res_obj
}

export const get_topics = () =>{
    var topics = []
    data.forEach(element=>{
        topics.push([element[1],element[4]])
    })
    topics = extract_duplicates(topics)
    var res_obj = []
    res_obj.push(
        {'id': '0','title': "kein Thema", 'text': '','color': '#ff0055' }
    )
    for(var i=0;i<topics.length;i++){
        res_obj.push(
            {'id': (i+1).toString(),'title': topics[i][0], 'text': topics[i][0], 'color': topics[i][1]}
        )
    }
    return res_obj
}

const extract_duplicates=(ar)=>{
    var res =[]
    ar.forEach(el=>{
        var inc = false
        for(var i=0;i<res.length;i++){
            if(el[0]==res[i][0]){
                inc = true
            }
        }
        if(!inc){
            res.push(el)
        }
    })

    return res
}