import { Dimensions, Platform, StyleSheet} from 'react-native';
import Constants from 'expo-constants';
const s_width = Dimensions.get('window').width
const s_height = Dimensions.get('window').height
const status_height = Platform.OS === 'ios' ? 20 : Constants.statusBarHeight;

export const styles = StyleSheet.create({
    container: {
      marginTop:status_height,
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
    input: {
      fontSize:16,
      backgroundColor: '#fff',
      borderColor: '#ff0055',
      borderRadius: s_width/39,
      borderWidth: s_width/98,
      padding: s_width/96,
      fontWeight: 'bold',
      fontSize:13,
      textAlign: 'center',
      margin:s_width/39,
      width:s_width*0.9,
      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 4,
      },
      shadowOpacity: 0.32,
      shadowRadius: 5.46,

      elevation: 9,
    }, 
    border: {
      borderColor:'#ff0055',
      borderRadius: s_width/39,
      borderWidth: s_width/78,
      margin:s_width/39,
      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 4,
      },
      shadowOpacity: 0,
      shadowRadius: 5,

      elevation: 5,
    },  
    footer: {
      backgroundColor:'rgba(255,255,255,.1)',
      marginTop:-s_height/60,
      padding:s_width/26,
      width:'110%',
      alignItems:'center',
      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 4,
      },
      shadowRadius: 4.65,

      elevation: 5,
    },
    switch: {
      position:'relative',
      alignSelf:'auto',
      marginHorizontal:s_width/39,
    },
    switch_text:{
      display:'flex',
      flexDirection:'row',
      alignSelf:'flex-start',
      marginLeft:s_width/39,
    },
    setting_text: {
      marginVertical:s_width/25,
      marginLeft:-s_width/39,
      color:'#888'
    },
    button: {
      backgroundColor:'#ff0055',
      borderColor:'#fff',
      borderRadius: s_width/39,
      borderWidth: s_width/98,
      paddingVertical:s_height/96,
      paddingHorizontal:s_width/13,
      margin:s_width/39,
      width:s_width*0.9,
      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 4,
      },
      shadowOpacity: 0.32,
      shadowRadius: 5.46,

      elevation: 9,
    },
    down: {
      position:'absolute',
      bottom:s_height/144,
    },
    button_text: {
      alignSelf:'center',
      fontWeight:'bold',
      color:'#fff',
      fontSize:16
    },
    img:{
      alignSelf:'center',
      maxHeight:s_height*0.5,
      maxWidth:s_width*0.7,
    },
    item: {
      backgroundColor:'#fff',
      borderWidth:0,
      borderRadius: s_width/98,
      borderLeftWidth:s_width/30,
      borderRightWidth:s_width/30,
      padding: s_width/39,
      fontWeight: 'bold',
      textAlign: 'center',
      width:s_width*0.9,
      width:s_width/1.12,
      alignSelf:'center'
    },
    topic: {
      backgroundColor:'#fff',
      borderRadius: s_width/39,
      borderWidth: s_width/98,
      padding: s_width/78,
      fontWeight: 'bold',
      textAlign: 'center',
      margin:s_width/39,
      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 4,
      },
      shadowOpacity: 0.32,
      shadowRadius: 5.46,

      elevation: 9,
      alignSelf:'center'
    },
    title: {
      fontSize:20,
      fontWeight:'bold',
      color:'#000',
      alignSelf:'center',
      lineHeight:s_height/25,
      textShadowOffset: {width: 0, height:0},
      textShadowRadius:5,
    },
    topic_text: {
      fontWeight:'bold',
      alignSelf:'center',
      textShadowOffset: {width: 0, height:0},
      textShadowRadius:1,
      textShadowColor:'#888',
      fontSize:16,
    },
    centeredView: {
      flex: 1,
      justifyContent: "center",
      alignItems: "center",
      marginTop:s_height/33,
    },
    modalView: {
      height:'105%',
      width:'100%',
      margin: s_width/20,
      marginBottom:s_height/15,
      backgroundColor: "white",
      borderRadius: s_width/20,
      padding: s_width/39,
      alignItems: "center",
      shadowColor: "#8c8c8c",
      shadowOffset: {
        width: 0,
        height: 12,
      },
      shadowOpacity: 0.58,
      shadowRadius: 16.00,
  
      elevation: 24,
    },
    textStyle: {
      color: "white",
      fontWeight: "bold",
      textAlign: "center"
    },
    modalText: {
      marginBottom: s_height/48,
      // textAlign: "justify",
      fontWeight:'bold',
      fontSize:18
    },
    scroll: {
      margin:s_width/39,
      marginBottom:s_height/5
    },
    h_list: {
      backgroundColor:'rgba(255,255,255,.1)',
      top:-s_height*0.08,
      width:'110%',
      height: s_height*0.2,
      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 4,
      },
      shadowOpacity: 0.30,
      shadowRadius: 4.65,
      elevation: 8,
      paddingTop:s_height/15,
      paddingHorizontal:s_width*0.07,
      marginBottom:-s_height*0.09,
    }

  });