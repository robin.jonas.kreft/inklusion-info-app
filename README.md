# Inklusion Info App

Die App kann als .apk Datei unter Releases heruntergreladen werden. 

Wenn ihr das Projekt lokal bei euch ausprobieren wollt, klont zunächst das git (oder ladet euch die .zip herunter und entpackt sie).
Außerdem wird [_nodejs_](https://nodejs.org/en/) benötigt (**Eine Version vor 17**). Das könnt ihr einfach herunterladen und installieren.

Ihr braucht schließlich noch die _expo-App_. Diese könnt ihr im [Playstore](https://play.google.com/store/apps/details?id=host.exp.exponent&hl=de&gl=US) oder [Appstore](https://apps.apple.com/de/app/expo-go/id982107779) finden. 

**Anschließend**:

1. navigiert über das Terminal in den Ordner mit dem Projekt und führt zunächst die folgenden Befehle aus:
    - `npm install --force`
    - `npm install expo-cli --global`
    - `npm install expo --global`
2. Führt das Projekt mit dem Befehl `expo start` aus. Alle weiteren Anweisungen seht ihr dann auf dem Bildschirm.
    - Bei der Ausführung wird ein neues Fenster in eurem Webbrowser geöffnet. Dort seht ihr, welche Geräte die App benutzen.
    - Bei Nutzern von MacOS kann während der Ausführung oder schon vorher eine Meldung erscheinen, die euch auffordert, die Developer-Tools         (X-Code) zu installieren. Dieses Fenster könnt ihr einfach schließen.
3. Scannt mit eurer Kamera(iPhone) oder in der expo-App(Android). Die App compiliert und öffnet sich daraufhin automatisch.
