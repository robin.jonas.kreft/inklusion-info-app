import React, { useEffect, useState } from 'react';
import { Modal, TouchableOpacity, FlatList, Text, View, Button, TextInput, ScrollView, Image, Switch } from 'react-native';
import * as Speech from 'expo-speech';
import * as Haptics from 'expo-haptics';
import { filter_data, get_topics } from './js/tools';
import { styles } from './js/styles';
import { get_text, get_tts } from './js/text';

export default function App() {

  const [data, setdata] = useState([]);
  const [search_item, setsearch] = useState("");
  const [topic, settopic] = useState("");
  const [selectedId, setSelectedId] = useState(null);
  const [selectedtopic, setselectedtopic] = useState('0');
  const [modalVisible, setModalVisible] = useState(false);
  const [modalText, setModalText] = useState("");
  const [modalTitle, setModalTitle] = useState("");
  const [t_data, set_t_data] =useState([]);
  const [vib, setVib] = useState(false);
  const [tts, setTts] = useState(false);

  const topics = []

  useEffect(() =>{
    set_t_data(get_topics())
  },[])

  useEffect(() =>{
    filter()
  },[topic])

  const Item = ({ item, onPress, borderColor, color }) => (
    <TouchableOpacity onPress={onPress} style={[styles.item, borderColor]}>
      <Text style={[styles.title, color]}>{item.title}</Text>
      <Text style={[styles.title]}>{item.chapter}</Text>
    </TouchableOpacity>
  );

  const Item_topics = ({ item, onPress, borderColor, color }) => (
    <TouchableOpacity onPress={onPress} style={[styles.topic, borderColor]}>
      <Text style={[styles.topic_text, color]}>{item.title}</Text>
    </TouchableOpacity>
  );

  const renderItem = ({ item }) => {
    const borderColor=item.color
    const color = item.color
    return (
      <View style={[styles.border, {borderColor}]}>
        <Item item={item} onPress={() =>{setSelectedId(item.id); setModalVisible(true); setModalText(item.text);setModalTitle(item.title); vibration('select')}} color={{color}} borderColor={{borderColor}}/>
      </View>
    );
  };

  const renderItem_topics = ({ item }) => {
    const color = item.id === selectedtopic ? item.color : '#999'
    const borderColor = item.id === selectedtopic ? item.color : '#999'
    topics.push(item.text)
    return (
      <Item_topics color={{color}} borderColor={{borderColor}} item={item} onPress={() =>{setselectedtopic(item.id); settopic(item.text); vibration('select')}}/>
    );
  };


  function vibration(type){
    if(vib){
      switch (type) {
        case 'select':
          Haptics.impactAsync()
          break;
        case 'search':
          Haptics.selectionAsync()

      }
    }
  }


  function search(){
    vibration('search')
    filter()
  }

  function filter(){
    var json_data = filter_data(search_item, topic)
    setdata(json_data)
  }

  function speak (){
    Speech.speak(modalTitle)
    Speech.speak(get_tts(modalText))
  }
 
  return (
    <View style={styles.container}>
      <View style={styles.h_list}>
        <FlatList data={t_data} renderItem={renderItem_topics} keyExtractor={item => item.id} extraData={selectedId} horizontal={true}/>
      </View>
        <FlatList data={data} renderItem={renderItem} keyExtractor={item => item.id} extraData={selectedId}/>
      <View style={styles.footer}>
        <TouchableOpacity style={styles.button} onPress={search}>
          <Text style={styles.button_text}>Suchen!</Text>
        </TouchableOpacity>
        <TextInput style={styles.input} value={search_item} onChangeText={setsearch} placeholder="gib einen Suchbegriff ein..."/>
        <View style={styles.switch_text}>
          <Switch style={styles.switch} trackColor={{ false: "#767577", true: "#f4f3f4" }} thumbColor={vib ? "#ff0055" : "#f4f3f4"} ios_backgroundColor="#3e3e3e" onValueChange={()=>setVib(!vib)} value={vib}/>
          <Text style={styles.setting_text}>haptisches Feedback</Text>
          <Switch style={styles.switch} trackColor={{ false: "#767577", true: "#f4f3f4" }} thumbColor={tts ? "#ff0055" : "#f4f3f4"} ios_backgroundColor="#3e3e3e" onValueChange={()=>setTts(!tts)} value={tts}/>
          <Text style={styles.setting_text}>Text to Speech</Text>
        </View>
        <TouchableOpacity onPress={()=>{setModalVisible(true); setModalText('about');setModalTitle('Über die App'); vibration('select')}} style={{backgroundColor:'#ff0055', borderColor:'#fff', borderRadius:7, borderWidth:3, shadowColor: "#000", shadowOffset: {width: 0,height: 4},shadowOpacity: 0.32, shadowRadius: 5.46, elevation: 9}}><Text style={{color:'#fff', fontWeight:'bold', fontSize:11, textAlign:'center', textAlignVertical:'center', padding:3}}>Über die App</Text></TouchableOpacity>
      </View>
      <Modal animationType="slide" transparent={true} visible={modalVisible} onRequestClose={() => {setModalVisible(!modalVisible);}}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <ScrollView style={styles.scroll}>
              <Text style={{fontSize:35, color:'#ff0055', fontWeight:'bold'}}>{modalTitle}</Text>
              {get_text(modalText)}
            </ScrollView>
            <View style={styles.down}>
              {tts && 
                <TouchableOpacity style={styles.button} onPress={speak}>
                <Text style={styles.button_text}>Vorlesen</Text>
              </TouchableOpacity>
              }
              <TouchableOpacity style={styles.button} onPress={() => {setModalVisible(!modalVisible); vibration('select'); Speech.stop()}}>
                <Text style={styles.button_text}>Schließen</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    </View>
  );
}


